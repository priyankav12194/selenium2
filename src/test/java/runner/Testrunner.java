package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src/test/java/login.feature",glue="steps",dryRun=false,snippets=SnippetType.CAMELCASE)
public class Testrunner extends AbstractTestNGCucumberTests{

}
