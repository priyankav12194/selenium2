package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class LoginSteps {
	
	public ChromeDriver driver;


	
	@Given("Launch the Browser")
	
	
	public void launchtheBrowser() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver= new ChromeDriver();
	}
	@And("Launch the url")
	public void  launchtheUrl()
	{	
	driver.get("http://leaftaps.com/opentaps/control/main");
	}
	@And ("Max the Window")
public void maxwindow() {
		driver.manage().window().maximize();
	}
	@And ("set the Timeouts")
	public void Settimeout() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	@And ("Enter  the username")
	public void enterusername()
	{
		driver.findElementById("username").sendKeys("Democsr");
	}
	@And (" Enter the Password")
	public void enterthepassword()
	{
	driver.findElementById("password").sendKeys("crmsfa");
}
	@When (" Login Successful")
public void login()
{
	driver.findElementByClassName("decorativeSubmit").click();
	System.out.println("Login successfully");
}

}