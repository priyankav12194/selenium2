package week4.day5;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import java.util.Date;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Project1zoomCar {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();//for launch the browser
		driver.get(" https://www.zoomcar.com/chennai");//for launch the url
		driver.manage().window().maximize();
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByLinkText("Start your wonderful journey").click();
	driver.findElementByXPath("//div[text()='Popular Pick-up points']/following::div[1]").click();
	//driver.findElementByXPath("(//div[@class='items'])[2]")	.click();
driver.findElementByClassName("proceed").click();
//Get the current date

Date date = new Date();

//Get only the date (and not month, year, time etc)
		
DateFormat sdf = new SimpleDateFormat("dd");

//Get today's date
		
String today = sdf.format(date);

//Convert to integer and add 1 to it

int tomorrow = Integer.parseInt(today)+1;

//Print tomorrow's date
		
System.out.println(tomorrow);
//driver.findElementByXPath("//div[contains(text(),"+tomorrow+")]");
driver.findElementByXPath("//div[text()='Sat']").click();
driver.findElementByClassName("proceed").click();
driver.findElementByClassName("proceed").click();


//driver.findElementByXPath("//div[@class='item active-input']").click();
//driver.findElementsByXPath("//div[@class='price']");


	}

}
