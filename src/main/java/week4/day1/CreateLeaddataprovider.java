package week4.day1;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week3.day2.Testcase.ProjectManager;

public class CreateLeaddataprovider  extends ProjectManager {
	@BeforeTest
	public void setData() {
		excelfilename = "Login";
	}
	@Test(dataProvider = "fetchData")
	public void createLead(String cn, String fn, String ln) {
		driver.findElement(By.linkText("Create Lead")).click();
		driver.findElement(By.id("createLeadForm_companyName")).sendKeys(cn);
		driver.findElement(By.id("createLeadForm_firstName")).sendKeys(fn);
		driver.findElement(By.id("createLeadForm_lastName")).sendKeys(ln);
		driver.findElement(By.name("submitButton")).click();
	}

}
