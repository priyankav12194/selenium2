package week1.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLeadscreation {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		//Launch the browser
			ChromeDriver driver=new ChromeDriver();
			
	//load the url
			driver.get("http://leaftaps.com/opentaps/control/login");
			//for manimise browser
			driver.manage().window().maximize();
			WebElement uname = driver.findElementById("username");
			uname.sendKeys("DemoSalesManager");
			//ENTER Pwd
			driver.findElementById("password").sendKeys("crmsfa");
			//enter login
			driver.findElementByClassName("decorativeSubmit").click();
			//enter logout
			//driver.findElementByClassName("decorativesubmit").click();
			//enter to click CRM/SFA
		driver.findElementByLinkText("CRM/SFA").click();
		//enter to click create lead
	driver.findElementByLinkText("Leads").click();
	driver.findElementByLinkText("Merge Leads").click();
	driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
	String parentWindow=driver.getWindowHandle();
	Set<String> winHandles = driver.getWindowHandles();
	System.out.println(driver.getTitle());
	List<String> listOfWindow=new ArrayList<String>();//for knowing count and jumping from one window to another window
	listOfWindow.addAll(winHandles);
	driver.switchTo().window(listOfWindow.get(1));
  // System.out.println("Parent window handle: " + parentWinHandle);
    // Locate 'Click to open a new browser window!' button using id
   WebElement newWindowBtn = driver.findElementByXPath("(//a[@class='linktext'])[1]");
   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
   newWindowBtn.click(); 
   driver.switchTo().window(parentWindow);
   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
     driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
    Set<String> win1Handles = driver.getWindowHandles();
 	System.out.println(driver.getTitle());
 	List<String> listOfWindow1=new ArrayList<String>();//for knowing count and jumping from one window to another window
 	listOfWindow1.addAll(win1Handles);
 	driver.switchTo().window(listOfWindow1.get(1));
    driver.findElementByXPath("(//a[@class='linktext'])[6]").click();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    driver.switchTo().window(parentWindow);
    driver.findElementByLinkText("Merge").click();
    Alert alertobj = driver.switchTo().alert();
   alertobj.accept();
   driver.findElementByXPath("(//a[@class='linktext'])[3]").click();
  	
	
		// TODO Auto-generated method stub

	}

}
