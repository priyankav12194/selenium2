package week1.day3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FindleadsCreation {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		//Launch the browser
			ChromeDriver driver=new ChromeDriver();
			
	//load the url
			driver.get("http://leaftaps.com/opentaps/*");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			//for manimise browser
			driver.manage().window().maximize();
			WebElement uname = driver.findElementById("username");
			uname.sendKeys("DemoSalesManager");
			//ENTER Pwd
			driver.findElementById("password").sendKeys("crmsfa");
			//enter login
			driver.findElementByClassName("decorativeSubmit").click();
			//enter logout
			//driver.findElementByClassName("decorativesubmit").click();
			//enter to click CRM/SFA
		driver.findElementByLinkText("CRM/SFA").click();
		//enter to click create lead
	driver.findElementByLinkText("Leads").click();
	driver.findElementByLinkText("Find Leads").click();
	driver.findElementByXPath("//input[@name='id']").sendKeys("10162");
	driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("vicky");
	driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("N");
	driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("CTS");
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	Thread.sleep(3000);
	//driver.manage().window().maximize();
	//WebElement button = driver.findElementByXPath("(//a[@class='linktext'])[4]");
	//button.click();
	//driver.manage().window().maximize();
	//browser.actions().mouseMove(nextButton).click().perform();
	driver.findElementByXPath("(//a[@class='linktext'])[4]").click();

driver.findElementByPartialLinkText("Delete").click();
	driver.findElementByLinkText("Find Leads").click();
	driver.findElementByXPath("//input[@name='id']").sendKeys("10160");
	driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("TestFirstName");
	driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("TestlastName");
	driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("TestCompany");
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	driver.findElementByXPath("(//a[@class='linktext'])[3]").click();
	/*String text = driver.findElement(By.id("viewLead_companyName_sp")).getText();
	System.out.println(text);
	driver.close();*/


	}

}
