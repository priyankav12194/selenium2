package week1.day3;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class Logincreation {

	private static Object allOptions;

	public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
	//Launch the browser
		ChromeDriver driver=new ChromeDriver();
		
//load the url
		driver.get("http://leaftaps.com/opentaps/*");
		//for manimise browser
		driver.manage().window().maximize();
		WebElement uname = driver.findElementById("username");
		uname.sendKeys("Democsr");
		//ENTER Pwd
		driver.findElementById("password").sendKeys("crmsfa");
		//enter login
		driver.findElementByClassName("decorativeSubmit").click();
		//enter logout
		//driver.findElementByClassName("decorativesubmit").click();
		//enter to click CRM/SFA
	driver.findElementByLinkText("CRM/SFA").click();
	//enter to click create lead
driver.findElementByLinkText("Leads").click();
driver.findElementByLinkText("Create Lead").click();
//create a first companyname

WebElement Cname = driver.findElementById("createLeadForm_companyName");
Cname.sendKeys("Microsoft");
//create a first name
driver.findElementById("createLeadForm_firstName").sendKeys("Priyaka");
//create a surname
driver.findElementById("createLeadForm_lastName").sendKeys("V");
//for Source dropdown
WebElement sourcedropdown = driver.findElementById("createLeadForm_dataSourceId");
Select sourcedropdown1=new Select(driver.findElementById("createLeadForm_dataSourceId"));
sourcedropdown1.selectByIndex(5);
//for local firstname
driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Priya");
//for local lastname
driver.findElementById("createLeadForm_lastNameLocal").sendKeys("L");
// for Salutation
driver.findElementById("createLeadForm_personalTitle").sendKeys("Welcome");
//for Title
driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Welcome to Testleaf");
//for Department
driver.findElementById("createLeadForm_departmentName").sendKeys("Private");
//for Annual Revenue
driver.findElementById("createLeadForm_annualRevenue").sendKeys("1,20,098");
//for preferred currency
WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
Select currencyobj= new Select(driver.findElementById("createLeadForm_currencyUomId"));
List<WebElement> currencyoptions = currencyobj.getOptions();
int size=currencyoptions.size();
currencyobj.selectByIndex(size-1);
for(WebElement eachOption:currencyoptions) {
		System.out.println(eachOption.getText());
		System.out.println("Total Number of item count in dropdown list = "  + size);
}
//for industry
WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
Select industry1=new Select(driver.findElementById("createLeadForm_industryEnumId"));
industry1.selectByIndex(7);
//for No.of employees
driver.findElementById("createLeadForm_numberEmployees").sendKeys("70");
//for Ownership
WebElement ownership = driver.findElementById("createLeadForm_ownershipEnumId");
Select ownership1=new Select(driver.findElementById("createLeadForm_ownershipEnumId"));
ownership1.selectByValue("OWN_LLC_LLP");
//for sic code
driver.findElementById("createLeadForm_sicCode").sendKeys("9876");
//for Ticker symbol
driver.findElementById("createLeadForm_tickerSymbol").sendKeys("IUY");
//for Description
driver.findElementById("createLeadForm_description").sendKeys("This is the best Company");
//for Important note
driver.findElementById("createLeadForm_importantNote").sendKeys("It is very best compaby for career developement");
//for country code
driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
//for area code
driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("44");
//for phone number
driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("78653225765");
//for extension
driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("765");
//for person to ask
driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("computer voice");
//for email
driver.findElementById("createLeadForm_primaryEmail").sendKeys("oiubvy@yahool.com");
//for weburl
driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://www.javatpoint.com/armstrong-number-in-java");
//for To Name
driver.findElementById("createLeadForm_generalToName").sendKeys("Venaktesan");
//for Attention
driver.findElementById("createLeadForm_generalAttnName").sendKeys("G");
//for AddressLine
driver.findElementById("createLeadForm_generalAddress1").sendKeys("3/712 kolavizhi amman nagar");
//for Address Line2
driver.findElementById("createLeadForm_generalAddress2").sendKeys("12th Street,Palavakkam");
//for City
driver.findElementById("createLeadForm_generalCity").sendKeys("Kanchipuram");
//for State
WebElement State = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
Select State12 =new Select(driver.findElementById("createLeadForm_generalStateProvinceGeoId"));
State12.selectByValue("MT");
driver.findElementById("createLeadForm_generalPostalCode").sendKeys("7886554");
WebElement Country = driver.findElementById("createLeadForm_generalCountryGeoId");
Select Country1 =new Select(driver.findElementById("createLeadForm_generalCountryGeoId"));
Country1.selectByIndex(16);
//for postal Extension
driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("98765");


//for dropdown createLeadForm_marketingCampaignId
WebElement eledropdown = driver.findElementById("createLeadForm_marketingCampaignId");
Select eledropdown1=new Select(driver.findElementById("createLeadForm_marketingCampaignId"));
eledropdown1.selectByVisibleText("Car and Driver");
eledropdown1.selectByValue("CATRQ_AUTOMOBILE");
List<WebElement> allOptions = eledropdown1.getOptions();
int size1 = allOptions.size();
eledropdown1.selectByIndex(size1-1);
for(WebElement eachOption:allOptions){
	System.out.println(eachOption.getText() );
}
driver.findElementByClassName("smallSubmit").click();
driver.findElementByPartialLinkText("Edit").click();
driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("P");
driver.findElementByClassName("smallSubmit").click();

 WebElement vname = driver.findElementById("viewLead_companyName_sp");
 vname.getText();
System.out.println(vname);
if (vname.equals(Cname))
		{
	System.out.println("match");
}else {
	System.out.println("not matched");
}
driver.findElementByLinkText("Find Leads").click();
driver.findElementByXPath("//input[@name='id']").sendKeys("10036");
//driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Vikcy");
//driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("N");
//driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("CTS");
//driver.findElementByXPath("//button[text()='Find Leads']").click();
driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
driver.findElementByLinkText("Merge Leads").click();
driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
/*driver.findElementByXPath("//input[@name='id']").sendKeys("10026");
driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Remo");
driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("Rock");
driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("Sony");
driver.findElementByXPath("//button[text()='Find Leads']").click();*/
driver.close();


}}