package practicecollections;

import java.util.ArrayList;

import net.bytebuddy.description.modifier.SynchronizationState;

public class List {

	public static void main(String[] args) {
		ArrayList<String> people=new ArrayList<String>();
		people.add("iron");
		people.add(1, "dog");
		people.add("cat");
		System.out.println("Arraylist is "+people);
		people.add(0, "lion");
		people.add("tiger");
		people.add("peacock");
		people.add(5, "wolf");
		
		System.out.println("after arraylist is"+people);
		for(String eachpeople: people) {
			System.out.println("foreach value"+eachpeople);
		}
	people.remove(0);
	people.remove("peacock");
	people.remove(4);
	people.set(2, "monkey");
	int pos=people.indexOf("monkey");
	System.out.println("index value is"+pos);
	System.out.println("after removing arraylist is "+people);
	boolean pos1=people.contains("peacock");
	System.out.println("after contains values is "+pos1);
	boolean pos2= people.contains("monkey");
	System.out.println("after contains values is"+pos2);
	people.clear();
	System.out.println("now array is "+people);
	
	}
}

