package week3.day3;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import week3.day2.Testcase.ProjectManager;

public class CreateLeadparameter extends ProjectManager{
	// @Test(invocationCount = 3, threadPoolSize = 4, timeOut = 20000)
		@Test(dataProvider = "fetchData"/* , dataProviderClass=week3.day2.ProjectM.class */)
		public void createLeadparameter(String cName, String fName, String lName) {

			driver.findElement(By.linkText("Create Lead")).click();
			driver.findElement(By.id("createLeadForm_companyName")).sendKeys(cName);
			driver.findElement(By.id("createLeadForm_firstName")).sendKeys(fName);
			driver.findElement(By.id("createLeadForm_lastName")).sendKeys(lName);
			driver.findElement(By.name("submitButton")).click();
		}

		@DataProvider(name = "fetchData")
		public String[][] getData() {
			String[][] data = new String[2][3];
			data[0][0] = "TestLeaf";
			data[0][1] = "Koushik";
			data[0][2] = "Chatterjee";

			data[1][0] = "TestLeaf";
			data[1][1] = "Mohan";
			data[1][2] = "B";

			return data;

			// String[] data = {"TestLeaf", "Koushik", "Chatterjee"};
		}

	}

