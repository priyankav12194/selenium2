package week3.day2classwork;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@Test
public class LearnAnnotations {
	@BeforeTest
	public void beforeTest() {
		System.out.println("@BeforeTest");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("@BeforeClass");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("@BeforeMethod");
	}
@Test
	public void run() {
		System.out.println("@run");
	}
@Test
	public void walk() {
		System.out.println("@Walk");
	}

	@AfterMethod
	public void AfterMethod() {
		System.out.println("@AfterMethod");
	}

	@org.testng.annotations.AfterClass
	public void AfterClass() {
		System.out.println("@AfterClass");
	}

	@AfterTest
	public void AfterTest() {
		System.out.println("@AfterTest");
	}

	@AfterSuite
	public void AfterSuite() {
		System.out.println("@AfterSuite");
	}

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("@BeforeSuite");
	}


}
