package week2.day3;


import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowhandle {

	public static void main(String[] args) throws IOException{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    ChromeDriver driver=new ChromeDriver();
		//to launch url
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.findElementByXPath("//a[text()[contains(.,'Contact Us')]]").click();
		Set<String>allwindow=driver.getWindowHandles();//for getting no of windows we use set
		System.out.println(driver.getTitle());//for title getting
		List<String> listOfWindow=new ArrayList<String>();//for knowing count and jumping from one window to another window
		listOfWindow.addAll(allwindow);
		driver.switchTo().window(listOfWindow.get(1));
		WebElement element=driver.findElementByXPath("//b[text()='Registered Office / Corporate Office : ']/following::p");
		String address=element.getText();
		System.out.println(address);
		//System.out.println("Registered Office / Corporate Office : ");
		//File scr = driver.getScreenshotAs(OutputType.FILE);
		//File desc=new File("C:/Users/Administrator/Pictures.jpg");
		//FileUtils.copyFile(scr, desc);
	}
	
	

}
