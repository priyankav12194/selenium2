package week2.day2;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Framecreation {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alertboxobj = driver.switchTo().alert();
		alertboxobj.sendKeys("Priya");
		alertboxobj.getText();
		System.out.println("text");
		alertboxobj.accept();
		String text = driver.findElementById("demo").getText();
System.out.println(text);
if (text.contains("Priya")) {
	System.out.println("match");
}else
	System.out.println("not matched");

		//driver.switchTo().frame("iframeResult");
driver.switchTo().defaultContent();



		// TODO Auto-generated method stub

	}

}
