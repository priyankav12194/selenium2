package week2.day2.homework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Iframecreationhomework {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.get("http://leafground.com/pages/frame.html");
	driver.manage().window().maximize();
	driver.switchTo().frame(0);
WebElement button = driver.findElementByXPath("//button[text()='Click Me']");
button.click();
System.out.println("");
System.out.println(button.getText());
button.click();	
System.out.println(button.getText());
System.out.println("");
driver.switchTo().defaultContent();
			//2frameClick
driver.switchTo().frame(1);

driver.switchTo().frame("frame2");
WebElement button1 = driver.findElementById("Click1");
button1.click();
System.out.println("");
System.out.println(button1.getText());
button1.click	();
System.out.println(button1.getText());
System.out.println("");
		
driver.switchTo().defaultContent();
//3 frame
driver.switchTo().frame(2);
driver.switchTo().frame("frame2");
WebElement button3 = driver.findElementByTagName("body");
String bodytext=button3.getText();
System.out.println(bodytext);
System.out.println("");
System.out.println("3main frame and 2 nestedframe and total is 5 frame");



	}

}
