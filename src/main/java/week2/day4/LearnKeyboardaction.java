package week2.day4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LearnKeyboardaction {

	public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
			//Launch the browser
				ChromeDriver driver=new ChromeDriver();
				
		//load the url
				driver.get("https://www.flipkart.com/");
				//for manimise browser
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);//WAIT FOR SECONDS
				driver.getKeyboard().sendKeys(Keys.ESCAPE);//for escape from the popup page
				WebElement Tvandappliances = driver.findElementByXPath("//span[text()='TVs & Appliances']");//for click tv and applicances
				Tvandappliances.click();//for clicking
				WebElement AC = driver.findElementByXPath("(//a[text()='Air Conditioners'])[1]");
				Actions builder = new Actions(driver);
				builder.moveToElement(Tvandappliances).perform();
				
				WebDriverWait wait=new WebDriverWait(driver,10);
				wait.until(ExpectedConditions.elementToBeClickable(Tvandappliances)).click();
				wait.until(ExpectedConditions.elementToBeClickable(AC)).click();
				

	}

}
