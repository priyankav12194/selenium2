package week2.day4.homework;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Selectable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		//Launch the browser
			ChromeDriver driver=new ChromeDriver();
			//launch the url
			driver.get("https://jqueryui.com/selectable/");
			driver.switchTo().frame(0);
			WebElement item1 = driver.findElementByXPath("//ol[@id='selectable']/li[1]");
			WebElement item3 = driver.findElementByXPath("//ol[@id='selectable']/li[3]");
			WebElement item5 = driver.findElementByXPath("//ol[@id='selectable']/li[5]");
			
			Actions builder=new Actions(driver);
			builder.keyDown(Keys.CONTROL).click(item1).click(item3).keyUp(Keys.CONTROL).click(item5).keyDown(Keys.CONTROL).perform();	// TODO Auto-generated method stub

	}

}
