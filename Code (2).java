import java.util.*;

public class Code {

	public static List<Integer> energy = new ArrayList<Integer>();
	public static List<Integer> strength = new ArrayList<Integer>();
	public static int N, T;

	public static void main(String[] args) {
		Scanner scan  = new Scanner(System.in);
		List<Boolean> WIN = new ArrayList<Boolean>();
		System.out.println("Enter number of Test Cases(1 < Test Case <= 10) - ");
		T=scan.nextInt();
		int i =0;
		while(i!=T){
			System.out.println("Enter equal number of Villians and Players(1 < Number <= 1000) - ");
			N= scan.nextInt();
			
			strength.clear();
			energy.clear();

			System.out.println("Enter strength values for Villians(1 < Strength <= 100000) - ");
			for(int j = 0; j<N; j++) {
				int temp = scan.nextInt();
				strength.add(j, temp);
			}

			System.out.println("Enter energy values for Players(1 < Energy <= 100000) - ");
			for(int j = 0; j<N; j++) {
				int temp = scan.nextInt();
				energy.add(j, temp);
			}
			sortList(strength);
			sortList(energy);
			WIN.add(compareList(N, strength, energy));
			i++;
		}
		scan.close();
		for (Boolean result : WIN) {
			if(result == true)
				System.out.println("WIN");
			else
				System.out.println("LOSE");
		}
	}

	public static void sortList(List<Integer> list) {

		Integer[] listArray = new Integer[list.size()];
		list.toArray(listArray);
		for(int i =0 ; i <listArray.length;i++) {
			for(int j = 0;j<listArray.length;++j) {
				if(listArray[i]<listArray[j]) {
					int temp=listArray[j];
					listArray[j]=listArray[i];
					listArray[i]=temp;
				}
			}
		}
		list.clear();
		for (Integer integer : listArray) {
			list.add(integer);
		}
	}

	public static boolean compareList(int n, List<Integer> str, List<Integer> enr) {

		boolean win = true;
		for(int i=0; i<N;i++) {
			if(strength.get(i)>energy.get(i)) {
				win= false; 
				break;
			}
			else
				continue;
		}
		return win;
	}
}